
# Definicao da funcao
def vrfPalindromo(str):
    return str == str[::-1]

# Principal
str = "teste"
res = vrfPalindromo(str)

# Resultados
if res:
    print("É Palíndromo")
else:
    print("Não é Palíndromo")
