
# Definicao da funcao
def vrfPrimos(var) : 
 
    # Corner cases 
    if (var <= 1) : 
        return False
    if (var <= 3) : 
        return True
 
    if (var % 2 == 0 or var % 3 == 0) : 
        return False
 
    i = 5
    while(i * i <= var) : 
        if (var % i == 0 or var % (i + 2) == 0) : 
            return False
        i = i + 6
 
    return True
 
# Chamada Principal
if (vrfPrimos(11)) : 
    print(" é primo.") 
else : 
    print(" não é primo.") 
     
if(vrfPrimos(15)) : 
    print(" é primo.") 
else : 
    print(" não é primo.") 
