# Termos
varTermos = int(input("Quantos termos?"))

# Configura Variaveis
varN1, varN2 = 0, 1
varCont = 0

# Verifica validade
if varTermos <= 0:
   print("Digite um número.")
elif varTermos == 1:
   print("Sequência Fibonacci até",varTermos,":")
   print(varN1)
else:
   print("Sequência:")
   while varCont < varTermos:
       print(varN1)
       varNT = varN1 + varN2
       # update values
       varN1 = varN2
       varN2 = varNT
       varCont += 1
